package entitys;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CoordinateTest {
	
	private static Coordinate coordinateTest;
	
	@BeforeAll
	private static void initilizeDefault() {
		coordinateTest = new Coordinate();
	}	
	
	@Test
	@DisplayName("Empiezan los tests")
	void defaultConstructorTest() {			
		assertTrue("El constructor por defecto no inicializa la coordenada al principio",
				   coordinateTest.getX() == 0 && coordinateTest.getY() == 0);
	}
	
	//Tests de constructores con datos correctos
	
	@ParameterizedTest(name = "{index} => x={0}, y={1}")
	@CsvSource( {"10, 5,", "7, 50,", "99, 100"} )
	public void genericConstructorTest(int x, int y) {
		Coordinate test = new Coordinate(x, y);		
		assertEquals(test.getX(), x);
		assertEquals(test.getY(), y);		
	}	
	
	@Test
	void cloneConstructorTest() {
		Coordinate clone = new Coordinate(coordinateTest);
		assertEquals("El constructor copia no clona el objeto debidamente", coordinateTest, clone);	
		
	}	
	
	@Test
	void getXTest() {
		int numTest = coordinateTest.getX();		
		assertEquals("El método getX() no devuelve el valor correcto", numTest, 0);		
	}
	
	@Test
	void getYTest() {
		assertEquals("El método getY() no devuelve el valor correcto", coordinateTest.getY(), 0);		
	}
	@Test
	void setXTest() {
		coordinateTest.setX(9);
		assertTrue("El método setX() no devuelve el valor correcto", coordinateTest.getX() == 9);
	}
	
	@Test
	void compareToTest() {
		Coordinate coordinate1 = new Coordinate(1,1);
		Coordinate coordinate2 = new Coordinate(1,1);
		Coordinate coordinate3 = new Coordinate(2,2);
		assertTrue(coordinate1.compareTo(coordinate2) == 0);
		assertTrue(coordinate1.compareTo(coordinate3) == -1);
		assertTrue(coordinate3.compareTo(coordinate1) == 1);
	}
}
